<?php

namespace Src\models;

use Src\helpers\Helpers;
use Src\models\DogModel;

class BookingModel {

	private $bookingData;
	private $helper;
	private $dogsData;

	function __construct() {
		$this->helper = new Helpers();
		$ruta = dirname(__DIR__) . '/../scripts/bookings.json';
		$rutaConBarraDiagonal = str_replace('\\', '/', $ruta);
		$string = file_get_contents($rutaConBarraDiagonal);
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings() {
		return $this->bookingData;
	}

	public function createBooking($data) {
		$bookings = $this->getBookings();

		$bookings[] = $data;
		$applyDiscount = $this->checkForDiscount($data['clientid']);
		if($applyDiscount) {
			$bookings['price'] = $bookings['price'] - ($bookings['price'] * 0.10);
		}

		$this->helper->putJson($bookings, 'bookings');

		return $data;
	}

	public function checkForDiscount($clientid) {
		$this->dogsData = new DogModel();
		$dogs = $this->dogsData->getDogs();
		$clientDogs = [];
		foreach ($dogs as $dog) {
			if ($dog['clientid'] == $clientid) {
				
			}
		}

		$dogsAvgAge = array_sum($clientDogs) / count($clientDogs);

		if($dogsAvgAge <= 10) {
			return true;
		} else {
			return false;
		}
	}
}